#ifndef MODULEINTERFACE_H
#define	MODULEINTERFACE_H

#include <qplugin.h>
#include <QThread>
#include <ToolsMessage.h>

#include "ToolsMessage.h"

class AppInterface : public QThread{
public:
    virtual void setCore(QObject *coreInterface) = 0;
    virtual void run() = 0;
    virtual void stop() = 0;
    virtual QString getID() = 0;
    virtual void receiveMessage(ToolsMessage& message) = 0;
    
protected:
    bool stopped;
    QString toolID;
};

Q_DECLARE_INTERFACE(AppInterface,
        "coder.tools.server.AppInterface/1.0")

#endif

