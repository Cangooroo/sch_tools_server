#ifndef CLIENTCOREINTERFACE_H
#define	CLIENTCOREINTERFACE_H

#include <QTcpSocket>
#include <QDir>
#include <ToolsMessage.h>

#include "AppInterface.h"

class ServerCoreInterface : public QObject{
    Q_OBJECT
public:   
    virtual void sendMessage(ToolsMessage& message) = 0;
    virtual void isClientConnected(QString clientName) = 0;
};

#endif
