#ifndef SCHAT_H
#define	SCHAT_H

#include <AppInterface.h>
#include <ServerCoreInterface.h>

class SCHat : public AppInterface{
    Q_OBJECT
    Q_INTERFACES(AppInterface)
public:
    SCHat();
    QString getID();
    void run();
    void stop();
    void receiveMessage(ToolsMessage& message);
    void setCore(QObject* coreInterface);
    
private:
    ServerCoreInterface *core;
};

#endif

