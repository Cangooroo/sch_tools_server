#include "SCHat.h"
#include <QDataStream>
#include <qplugin.h>

SCHat::SCHat() {
    toolID = "APP_SCHAT";
    stopped = false;
    core = NULL;
}

QString SCHat::getID() {
    return toolID;
}


void SCHat::run() {
    while(!stopped){
        sleep(5);
    }
}

void SCHat::stop() {
    stopped = true;
}

void SCHat::setCore(QObject* coreInterface) {
    core = (ServerCoreInterface*) coreInterface;
}

void SCHat::receiveMessage(ToolsMessage& message) {
    qDebug() << "[SCHat] Received message, transfering it to" << message.to;
    core->sendMessage(message);
} 

Q_EXPORT_PLUGIN2(SCHat, SCHat); // (plugin name, plugin class) 