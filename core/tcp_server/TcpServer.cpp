#include "TcpServer.h"

TcpServer::TcpServer(QObject* parent) : QTcpServer(parent){
    
}

void TcpServer::incomingConnection(int handle) {
    emit this->passConnection(handle);
}
