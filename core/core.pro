QMAKE_CXXFLAGS += -std=c++11
INCLUDEPATH = ../interfaces
SOURCES    = src/*.cpp tcp_server/*.cpp
HEADERS    = src/*h tcp_server/*.h ../interfaces/*.h
TARGET     = sch_tools_server
DESTDIR    = ../build
QT += core network
