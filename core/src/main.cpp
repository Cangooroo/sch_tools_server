#include <QCoreApplication>
#include <QDir>
#include <QTime>

#include "ServerCore.h"

int main(int argc, char *argv[]) {
    QCoreApplication app(argc, argv);
    
    srand(QDateTime::currentMSecsSinceEpoch());
    
    QDir appDir(qApp->applicationDirPath());
    appDir.cd("tools");
    ServerCore server;
    server.setAppDir(appDir);
    server.loadApp("SCHat");
    QObject::connect(&server, SIGNAL(exit()), &app, SLOT(quit()));
    
    return app.exec();
}