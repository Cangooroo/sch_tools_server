#ifndef SERVERCORE_H
#define	SERVERCORE_H

#include <QTcpSocket>
#include <QMultiHash>
#include <QSocketNotifier>
#include "../tcp_server/TcpServer.h"
#include "Client.h"
#include <ServerCoreInterface.h>

class ServerCore : public ServerCoreInterface {
    Q_OBJECT
    
private:
    
    typedef QMultiHash<QString, Client*> Clients;
    
    class CLICommandParserBase {
    public:
        /**
         * Megnézi, hogy végre tudja-e hajtani a megadott parancsot.
         * @param A megadott parancs első szava.
         * @return True, ha végre tudja hajtani, false egyébként.
         */
        virtual bool canExecute(QString/*első szó*/) = 0;
        
        /**
         * Végrehajtja a parancsot.
         * @param A parancs (az első szó kivételével).
         * @return True, ha sikeresen lefutott, false egyébként.
         */
        virtual bool execute(QString/*a többi szó*/) = 0;
        
        static void setCore(ServerCore *serverCore){
            core = serverCore;
        }
    protected:
        static ServerCore *core;                   
    };
    
public:
    ServerCore();
    void setAppDir(const QDir& applicationDir);
    bool loadApp(const QString& appName);
    bool unloadApp(const QString& appID);
    void sendMessage(ToolsMessage& message);
    void isClientConnected(QString clientName);

private:
    /**
     * ServerCore-nak szóló üzenet. Azért kell, hogy ne legyen től nagy
     * a readClient() függvény
     * @param message A readClient által beolvasott ToolsMessage
     * @param clientSocket A kliensek kezeléséhez szükséges pointer az aktuális
     * kliensre
     */
    void messageToCore(ToolsMessage& message, QTcpSocket* clientSocket);
    
    /**
     * Visszaadja a kliens nevét a socket alapján
     * @param alsoBiggerThan79Columns A kliens socketje
     * @return A kliens neve
     */
    QString todoMultiHashFindSCHAccByQTcpSocketPointer_thisIsUgly(QTcpSocket *alsoBiggerThan79Columns);

protected slots:
    void incomingConnection(int handle);
    void closeConnection();
    void readClient();
    void error();
    void readCLI();
    void doExit();

private:
    Clients clients;   // schacc -> socket
    TcpServer server;
    QDir appDir;
    QMap<QString, AppInterface*> apps;
    QSocketNotifier *notifier;
    QSet<CLICommandParserBase*> commandParsers;
    
signals:
    void exit();

private:
    /*
     TODO parancs osztályok
     */
    class LoadToolParser : public CLICommandParserBase{ 
        // Betölt egy toolt
        virtual bool canExecute(QString command);
        virtual bool execute(QString command);
    };
    
    class UnloadToolParser : public CLICommandParserBase{
        // Unloadol egy toolt
        virtual bool canExecute(QString command);
        virtual bool execute(QString command);
    };
    
    class LoadedToolsParser : public CLICommandParserBase{ 
        // Kiírja a betöltött(szerver oldali toolokat)
        virtual bool canExecute(QString command);
        virtual bool execute(QString command);
    };
    
    class ConnectedClientsParser : public CLICommandParserBase{
        // Kiírja a kapcsolódott kliensek számát.
        virtual bool canExecute(QString command);
        virtual bool execute(QString command);
    };
    
    class ExitParser : public CLICommandParserBase{
        // Kiírja a kapcsolódott kliensek számát.
        virtual bool canExecute(QString command);
        virtual bool execute(QString command);
    };
};

#endif	
