#ifndef CLIENT_H
#define	CLIENT_H

struct Client {
    QSet<QString> enabledTools;
    QTcpSocket* socket;
};

#endif	/* CLIENT_H */

