#include <iostream>

#include <QPluginLoader>
#include <ToolsMessage.h>
#include <QDataStream>
#include <qt4/QtCore/qbytearray.h>
#include "ServerCore.h"

ServerCore* ServerCore::CLICommandParserBase::core;

ServerCore::ServerCore() {
    if (!server.listen(QHostAddress::Any, 14000)) {
        qDebug() << "[Initialization] Failed to bind to port 14000";
    } else {
        qDebug() << "[Initialization] Succesfully binded to port 14000";
        connect(&server, SIGNAL(passConnection(int)),
                this, SLOT(incomingConnection(int)));
    }
    notifier = new QSocketNotifier(fileno(stdin), QSocketNotifier::Read, this);
    connect(notifier, SIGNAL(activated(int)), this, SLOT(readCLI()));
    
    CLICommandParserBase::setCore(this);    
    ExitParser *exitParser = new ExitParser();
    commandParsers.insert(exitParser);
    commandParsers.insert(new LoadToolParser());
    commandParsers.insert(new UnloadToolParser());
    commandParsers.insert(new LoadedToolsParser());
    commandParsers.insert(new ConnectedClientsParser());
}

void ServerCore::incomingConnection(int handle) {
    Client *client = new Client();
    QTcpSocket *clientSocket  = new QTcpSocket(this);
    clientSocket->setSocketDescriptor(handle);
    connect(clientSocket, SIGNAL(readyRead()),
            this, SLOT(readClient()));
    connect(clientSocket, SIGNAL(disconnected()),
            this, SLOT(closeConnection()));
    client->socket = clientSocket;
    clients.insert("guest", client);
    qDebug() << "[Network] New connection from" << (clientSocket->peerAddress().toString());
}

void ServerCore::closeConnection() {
    QTcpSocket* clientSocket = (QTcpSocket*) sender();
    Clients::iterator it = clients.begin();
    while(it != clients.end()){
        if(it.value()->socket==clientSocket){
            qDebug() << "[Network] Client" << 
                    it.key() << "disconnected!";
            clients.remove(it.key(), it.value());
            break;
        }
        ++it;
    }
}

void ServerCore::readClient() {
    QTcpSocket* clientSocket = (QTcpSocket*) sender();
    QDataStream in(clientSocket);
    in.setVersion(QDataStream::Qt_4_8);

    // Message beolvasása
    ToolsMessage message;
    in >> message.toolID >> message.to >> message.from >> message.data;
    
    // Mesage továbbküldése
    if(message.to=="core"){
        // ServerCore-nak szóló üzenet
        messageToCore(message, clientSocket);
    } else{
        // Tool-nak szóló üzenet
        Clients::iterator it = clients.begin();
        while(it!=clients.end()){
            if(clientSocket == it.value()->socket && it.key()!="guest"){
                if(apps.contains(message.toolID)){
                    apps[message.toolID]->receiveMessage(message);
                } else {
                    qDebug() << "[Network in] Got message to send to "
                            "[toolID:" << message.toolID << "], but no suck toolID!";
                }
                                            // kiolvas minden hátralévő adatot
                    clientSocket->readAll();// (pl. nem tudtuk értelmezni a csomagot,
                                            // de van még benne x byte)
                    return;
            }
            it++;
        }
        qDebug() << "[Network in] Can't dispatch message, the client is not "
                "logged in! schacc: " << message.from;
    }
                                // kiolvas minden hátralévő adatot
    clientSocket->readAll();    // (pl. nem tudtuk értelmezni a csomagot,
                                // de van még benne x byte)
}

void ServerCore::messageToCore(ToolsMessage& message, QTcpSocket* clientSocket) {
    QString tmp;
    QDataStream stream(message.data);
    stream >> tmp;
    qDebug() << tmp << " wat";
    if(tmp=="sign_in"){
        qDebug() << "[Network in] Got a sign in request from " << message.from;
        Clients::iterator i = clients.find("guest");
        while(i!=clients.end() && i.key()=="guest"){
            if(i.value()->socket==clientSocket){
                Client *tempClient = i.value();
                clients.remove(i.key(), i.value());
                clients.replace(message.from, tempClient);
                /*
                 * Visszaválaszolunk a kliensnek.
                 */
                ToolsMessage reply;
                QDataStream out(&(reply.data), QIODevice::ReadWrite);
                out << QString("login_success");
                
                reply.to = message.from;
                reply.from = "core";
                reply.toolID = "CORE";
                sendMessage(reply);
                
                break;
            }
            ++i;
        }
    } else if(tmp=="tool_loaded") {
        QString toolName; 
        stream >> toolName;  
        QString schacc = todoMultiHashFindSCHAccByQTcpSocketPointer_thisIsUgly(clientSocket);
        qDebug() << "[Network in] Got a loaded tool [" << toolName << "] from " << schacc;
//        clients[schacc]->enabledTools.insert(toolName);
    } else if(tmp=="tool_unloaded") {
        QString toolName; 
        stream >> toolName;
        QString schacc = todoMultiHashFindSCHAccByQTcpSocketPointer_thisIsUgly(clientSocket);
        qDebug() << "[Network in] Got an unloaded tool [" << toolName << "] from " << schacc; 
//        clients[schacc]->enabledTools.remove(toolName);
    }  else {
        qDebug() << "[Network in] Can't change socket name to"
                << message.from;
    }
}

bool ServerCore::loadApp(const QString& appName){
    QString file = appName + ".app";
    QPluginLoader pluginLoader(appDir.absoluteFilePath(file));
    qDebug() << "[ToolLoader] Going to load tool from " << appDir.absoluteFilePath(file);;
    QObject *plugin = pluginLoader.instance();
    if (plugin) {
        AppInterface *appInstance = qobject_cast<AppInterface *>(plugin);
        if (appInstance) {
            apps.insert(appInstance->getID(), appInstance);
            qDebug() << "[ToolLoader] Loaded tool witd toolID: " << appInstance->getID();
            appInstance->setCore(this);
            appInstance->start();
        } else {
            qDebug() << "[ToolLoader] Error while loading module: " << pluginLoader.errorString() << " !";
            return false;
        }
    } else{
        qDebug() << "[ToolLoader] Can't load plugin!";
    }
    return true;
}

bool ServerCore::unloadApp(const QString& appID){
    qDebug() << "[ToolUnLoader]ClientCore is going to unload " << appID << "!";
    if (apps.contains(appID)) {
        apps[appID]->stop();
        apps.remove(appID);
    } else {
        qDebug() << "[ToolUnLoader]" << appID << "is not among the loaded apps!";
    }
    return true;
}

void ServerCore::error() {
//    qDebug() << "ClientCore Socket Error: " << clientSocket->errorString();
}

void ServerCore::setAppDir(const QDir& applicationDir) {
    appDir = applicationDir;
}

void ServerCore::sendMessage(ToolsMessage& message) {
    QByteArray out;
    QDataStream stream(&out, QIODevice::ReadWrite);
    stream << message.toolID << message.to << message.from << message.data;
    if(message.to=="all"){
        for(QString current_key : clients.keys()){
            if(current_key!="guest"){
                Clients::iterator it = clients.find(current_key);
                it.value()->socket->write(out);
                qDebug() << "[Network out] Message sent to sch_acc " << it.key();
            }
        }
    } else{
        if(clients.contains(message.to)){
            Clients::iterator it = clients.find(message.to);
            it.value()->socket->write(out);
            qDebug() << "[Network out] Message sent to sch_acc " << message.to << ":" << message.toolID;
        } else{
            qDebug() << "[Network out] Can't send to sch_acc " <<
                    message.to << ". Not amongst the clients.";
        }
    }
}

QString ServerCore::todoMultiHashFindSCHAccByQTcpSocketPointer_thisIsUgly(QTcpSocket* alsoBiggerThan79Columns) {
    Clients::iterator it = clients.begin();
    while(it!=clients.end()){
        if(it.value()->socket == alsoBiggerThan79Columns){
            return it.key();
        }
        it++;
    }
    return QString();
}

void ServerCore::isClientConnected(QString clientName) {
    // TODO
}

void ServerCore::readCLI() {
    QTextStream stream(stdin, QIODevice::ReadOnly);
    stream.setCodec("UTF-8");
    QString temp;
    stream >> temp;
    for(QSet<CLICommandParserBase*>::iterator it = commandParsers.begin();it!=commandParsers.end();it++){
        if((*it)->canExecute(temp)){
            (*it)->execute(stream.readLine());
        }
    }
    std::cout << " >>> semmu@SCHTools ~ $ ";
    std::flush(std::cout);
}

void ServerCore::doExit() {
    emit exit();
}

// -------------- Can Execute

bool ServerCore::LoadToolParser::canExecute(QString command) {
    return command == "load";
}

bool ServerCore::LoadedToolsParser::canExecute(QString command) {
    return command == "anyád";
}

bool ServerCore::UnloadToolParser::canExecute(QString command) {
    return command == "anyád3";
}

bool ServerCore::ConnectedClientsParser::canExecute(QString command) {
    return command == "anyád4őúüóűé";
}

bool ServerCore::ExitParser::canExecute(QString command) {
    return command == ":wq" || command == ":q!" || command == "exit";   // TODO valami. nem tudjuk. de fontosasd
    
}

// ------------- Execution

bool ServerCore::LoadToolParser::execute(QString command) {
    QTextStream stream(&command, QIODevice::ReadOnly);
    stream.read(1); // kezdő space eldobása pl.:  load asdasd ----> load " asdasd" lenne
    qDebug() << "[CLI] Loading tool: " << command;
    return true;
}

bool ServerCore::LoadedToolsParser::execute(QString command) {
    qDebug() << "anyádat";
    return true;
}

bool ServerCore::UnloadToolParser::execute(QString command) {
    return false;
}

bool ServerCore::ConnectedClientsParser::execute(QString command) {
    return false;
}

bool ServerCore::ExitParser::execute(QString command) {
    qDebug() << "[CLI] Exiting...";
    emit core->exit();
    return true;
}
